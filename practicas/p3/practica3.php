<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<?php
    $a="ManejadorSQL";
    $b='MySQL';
    $c=&$a;
?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <?php
            echo "<title>Práctica 3: Juan P. Mendoza</title>";
        ?>
    </head>
    <body>
         
        <?php
            echo "<p>Variable a: $a</p>";
            echo "<p>Variable b: $b</p>";
            echo "<p>Variable c: $c</p>";
        ?>
        
        <?php
            $a="PHPServer";
            $b=&$a;
        ?>
        
        <?php
            echo "------------------------";
            echo "<p>Variable a: $a</p>";
            echo "<p>Variable b: $b</p>";
            echo "<p>Variable c: $c</p>";
        ?>
        
        <?php
            echo "------------------------";
            echo "<p>La razón por la que todas las variables cambiaron en la segunda asignación, es porque tanto la variable b como la c estaban haciendo referencia a la variable a; entonces, al hacer un cambio a la variable a, las variables que estaban referenciando a esta también cambian.</p>";
        ?>

        <?php
            echo "------------------------";
            $a = "PHP5";
            echo "<p>Variable a: $a</p>";
            $z[] = &$a;
            echo "Variable z:";
            var_dump($z);
            echo "<br>";
            $b = "5a versión de PHP";
            echo "<p>Variable b: $b</p>";
            $c = $b*10;
            echo "<p>Variable c: $c</p>";
            $a .= $b;
            echo "<p>Variable a: $a</p>";
            $b *= $c;
            echo "<p>Variable b: $b</p>";
            $z[0] = "MySQL";
            echo "Variable z:";
            var_dump($z);
            echo "<br>";
        ?>

        <?php
            echo "------------------------<br>";
            echo "Variable a de forma global: " . $GLOBALS["a"] . "<br>";
            echo "Variable b de forma global: " . $GLOBALS["b"] . "<br>";
            echo "Variablc de forma global: " . $GLOBALS["c"] . "<br>";
            echo "Variable z de forma global: " . $GLOBALS["z"] . "<br>";
        ?>

        <?php
            echo "------------------------<br>";
            $a = "7 personas";
            $b = (integer) $a;
            $a = "9E3";
            $c = (double) $a;
        ?>
    </body>
</html>